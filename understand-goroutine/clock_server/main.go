package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"time"
)

var (
	ind int
)

func main() {

	port := flag.Int("port", 8000, "an int")
	flag.Parse()
	fmt.Println("port: ", *port)
	listener, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", *port))
	if err != nil {
		log.Fatal(err)
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Print(err)
			continue
		}
		// handleConn(conn) # example of sequential server
		go handleConn(conn) // # example of concurrent
	}
}

func handleConn(conn net.Conn) {
	ind = ind + 1
	fmt.Printf("I am the number %d goroutine\n", ind)
	defer conn.Close()
	for {
		_, err := io.WriteString(conn, time.Now().Format("Mon Jan 2 15:04:05\n"))
		if err != nil {
			return
		}
		time.Sleep(1 * time.Second)
	}
}
