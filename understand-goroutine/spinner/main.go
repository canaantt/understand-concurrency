package main

import (
	"fmt"
	"time"
)

func main() {
	go spinner(1000 * time.Millisecond)
	const n = 45
	fibN := fib(n)
	fmt.Printf("\rFibonacci(%d) = %d\n", n, fibN)
}

func spinner(delay time.Duration) {
	for {
		for _, r := range `-\|/` {
			fmt.Printf("%c", r)
			time.Sleep(delay)
		}
	}
}

func fib(number int) int {
	if number < 2 {
		return number
	}
	return fib(number-1) + fib(number-2)
}
